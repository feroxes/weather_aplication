## Weather application

<a href="https://feroxes.gitlab.io/weather-app/">Demo</a>

This is an application, written on React.js + Redux. 
It shows current weather and weather forecast.
You can choose any city to view the weather.

Enjoy!

To run this app on local machine you should: 
1. `clone` this repo;
2. being on app's folder `npm i` or `yarn`
3. then `npm start` or `yarn start`
